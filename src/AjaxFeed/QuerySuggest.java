/*
 *   Copyright © 2015 Team Bees
 *   Dongguang You - u3503025@hku.hk,
 *   Robert Long - rbtLong@live.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  DISCLAIMER
 *  Please DO NOT copy any of the contents from our source code to use
 *  for an assignment. We are not allowing this under any circumstances.
 *  We will not take responsibility for the consequences relating to
 *  academic dishonesty and/or plagarism.
 */

package AjaxFeed;

import SearchDb.DbCtx;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rbtlong on 6/7/15.
 */
@WebServlet(name = "QuerySuggest")
public class QuerySuggest extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    private static String toJsonSingle(String name, List<String> values){
        String elms = "";
        for(int i=0; i<values.size(); ++i)
            elms += String.format("\"%s\"%s",
                    values.get(i),
                    i == values.size() -1 ? "" : ", ");
        return String.format("{ \"%s\" : [ %s ] }", name, elms);
    }

    private static String combineAllButLast(String[] s){
        if(s.length < 2) return "";
        else {
            String comb = "";
            for(int i=0; i<s.length-1; ++i)
                if(i != s.length-1)
                    comb += String.format("%s ", s[i]);
            return comb;
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ArrayList<String> sugg = new ArrayList<>();
        if(request.getParameter("q") != null){
            String[] q = request.getParameter("q").split("\\s");
            String allButLast = combineAllButLast(q);
            if(q.length > 0) {
                String term = q[q.length - 1];
                try {
                    for(String t : DbCtx.fetchSuggestions(term, 10)){
                        sugg.add(allButLast + t);
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                } catch (NamingException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }

        response.setContentType("application/json");
        response.getWriter().write(toJsonSingle("suggest", sugg));
    }
}
