/*
 *   Copyright © 2015 Team Bees
 *   Dongguang You - u3503025@hku.hk,
 *   Robert Long - rbtLong@live.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  DISCLAIMER
 *  Please DO NOT copy any of the contents from our source code to use
 *  for an assignment. We are not allowing this under any circumstances.
 *  We will not take responsibility for the consequences relating to
 *  academic dishonesty and/or plagarism.
 */

package Core;

import Core.Models.SearchResult;
import SearchDb.DbCtx;

import javax.naming.NamingException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SearchEngine {
    String[] terms;

    public SearchEngine(String[] terms){
        this.terms = terms;
    }

    public List<SearchResult> tfidfSearch(int limit, int offset)
            throws SQLException, NamingException, ClassNotFoundException {
        prepareTfIdfSearch();

/*         ResultSet rsLen = DbCtx.getDocIdRankingLength()
                .executeQuery();
        rsLen.next();
        querySize = rsLen.getInt(1);*/

        ResultSet rs = DbCtx.getDocIdRankingsTfIdf(limit, offset).executeQuery();
        ArrayList<SearchResult> results = new ArrayList<>();

        while(rs.next())
            results.add(new SearchResult(rs));

        return results;
    }

    public List<SearchResult> lsaSearch(int limit, int offset)
            throws SQLException, NamingException, ClassNotFoundException {
        prepareLSASearch();

/*        ResultSet rsLen = DbCtx.getDocIdRankingLengthLSA()
                .executeQuery();
        rsLen.next();
        querySize = rsLen.getInt(1);*/

        ResultSet rs = DbCtx.getDocIdRankingsLSA(limit, offset).executeQuery();
        ArrayList<SearchResult> results = new ArrayList<>();
        while(rs.next())
            results.add(new SearchResult(rs));
        return results;
    }


    private void prepareTfIdfSearch()
            throws SQLException, ClassNotFoundException, NamingException {
        DbCtx.dropQueryTfIdf().executeUpdate();
        DbCtx.createQueryTfIdf().executeUpdate();
        computeQueryByTfIdf(terms);
    }
    private void prepareLSASearch()
            throws SQLException, ClassNotFoundException, NamingException {
        DbCtx.dropQueryTfIdf().executeUpdate();
        DbCtx.createQueryTfIdf().executeUpdate();
        computeQueryByLSA(terms);
    }

    private void computeQueryByTfIdf(String[] terms)
            throws SQLException, ClassNotFoundException, NamingException {

        boolean[] repeat= new boolean[terms.length];
        double[] scores = new double[terms.length];
        int[] termids= new int[terms.length];
        for(int i=0;i<repeat.length;i++){
            repeat[i]=false;
            scores[i]=0.0;
        }
        for(int i=0;i<terms.length;i++){
            if(!repeat[i]){
                scores[i]=1.0;
                for(int j=i+1;j<terms.length;j++) {
                    if (terms[j].equals(terms[i])){
                        repeat[j]=true;
                        scores[i]+=1.0;
                    }
                }
                scores[i]= 1+Math.log(scores[i]);
            }
        }
        double length=0.0;
        int df=0;
        for(int i=0;i<terms.length && !repeat[i];i++){
            df=1;
            ResultSet rs = DbCtx.getID(terms[i]).executeQuery();
            if(rs.next()){
                //df+=rs.getInt(1);
                termids[i]=rs.getInt(1);
            }
            else{
                repeat[i]=true;
                break;
            }
            rs= DbCtx.getDF(termids[i]).executeQuery();
            if(rs.next()) {
                df = rs.getInt(1);
            }
            scores[i]*= Math.log(42498/df);
            length+=scores[i]*scores[i];
        }
        length=Math.sqrt(length);
        for(int i=0;i<terms.length && !repeat[i];i++){
            scores[i]/=length;
            DbCtx.PopQueryTfIdf(termids[i],scores[i]).executeUpdate();
        }
    }

    private void computeQueryByLSA(String[] terms)
            throws SQLException, ClassNotFoundException, NamingException {
        computeQueryByTfIdf(terms);
        DbCtx.dropQueryLSA().executeUpdate();
        DbCtx.createQueryLSA().executeUpdate();
        DbCtx.populateQueryLSA().executeUpdate();

    }

}
