/*
 *   Copyright © 2015 Team Bees
 *   Dongguang You - u3503025@hku.hk,
 *   Robert Long - rbtLong@live.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  DISCLAIMER
 *  Please DO NOT copy any of the contents from our source code to use
 *  for an assignment. We are not allowing this under any circumstances.
 *  We will not take responsibility for the consequences relating to
 *  academic dishonesty and/or plagarism.
 */

package Core.Models;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SearchResult {
    int docId;
    String title;
    String url;
    double score;

    public int getDocId() {
        return docId;
    }

    public void setDocId(int docId) {
        this.docId = docId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public SearchResult(int docId, String title, String url, double score) {
        this.docId = docId;
        this.title = title;
        this.url = url;
        this.score = score;
    }

    public SearchResult(ResultSet rs)
            throws SQLException {
            docId = rs.getInt("id");
            title = rs.getString("url");
            url = rs.getString("url");
            score = rs.getDouble("score");

    }

    @Override
    public String toString() {
        return String.format("[%s] (%.4f) <a href='http://%s' target='_blank'>%s</a>",
                docId, score, url, title);
    }

    public String toStringAsTd(){
        return String.format(
                "<td>%s</td>" +
                "<td>%.4f</td>" +
                "<td><a href='http://%s' target='_blank'>%s</a></td>",
                docId, score, url, title);
    }
}
