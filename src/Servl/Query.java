/*
 *   Copyright © 2015 Team Bees
 *   Dongguang You - u3503025@hku.hk,
 *   Robert Long - rbtLong@live.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  DISCLAIMER
 *  Please DO NOT copy any of the contents from our source code to use
 *  for an assignment. We are not allowing this under any circumstances.
 *  We will not take responsibility for the consequences relating to
 *  academic dishonesty and/or plagarism.
 */

package Servl;

import Core.Models.SearchResult;
import Core.SearchEngine;

import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.stream.Collectors;

public class Query extends javax.servlet.http.HttpServlet {
    protected void doPost(javax.servlet.http.HttpServletRequest request,
                          javax.servlet.http.HttpServletResponse response)
            throws javax.servlet.ServletException, IOException {

    }

    protected void doGet(javax.servlet.http.HttpServletRequest request,
                         javax.servlet.http.HttpServletResponse response)
            throws javax.servlet.ServletException, IOException {
        int limit = 20;
        int offset = 1;
        ServletContext ctx = getServletContext();
        RequestDispatcher disp = ctx.getRequestDispatcher("/Results.jsp");
        if(request.getParameter("q") != null) {
            String[] terms = request.getParameter("q").split("\\s");
            ArrayList<String> results = new ArrayList<>();

            String spOffset = request.getParameter("offset");
            if(spOffset != null && spOffset.matches("\\d*"))
                offset = Integer.parseInt(spOffset);

            String spLimit = request.getParameter("limit");
            if(spLimit != null && spLimit.matches("\\d*")) {
                int tspLimit = Integer.parseInt(spLimit);
                if(tspLimit > 0 && tspLimit <= 100)
                    limit = tspLimit;
            }

            try {
                if(request.getParameter("qMode") != null
                        && request.getParameter("qMode").equals("lsa")){
                    SearchEngine se = new SearchEngine(terms);
                    results.addAll(se.lsaSearch(limit, offset)
                            .stream().map(SearchResult::toStringAsTd)
                            .collect(Collectors.toList()));

                    //request.setAttribute("qsize", se.getQuerySize());
                } else if( request.getParameter("qMode") != null
                        && request.getParameter("qMode").equals("both")) {

                } else {
                    SearchEngine se = new SearchEngine(terms);
                    results.addAll(se.tfidfSearch(limit, offset)
                            .stream().map(SearchResult::toStringAsTd)
                            .collect(Collectors.toList()));

                    //request.setAttribute("qsize", se.getQuerySize());
                }

            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (NamingException e) {
                e.printStackTrace();
            }

            request.setAttribute("qresult", results);
            disp.forward(request, response);


        } else
            response.getWriter().write("No query present.");
    }

}
