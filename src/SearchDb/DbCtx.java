/*
 *   Copyright © 2015 Team Bees
 *   Dongguang You - u3503025@hku.hk,
 *   Robert Long - rbtLong@live.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  DISCLAIMER
 *  Please DO NOT copy any of the contents from our source code to use
 *  for an assignment. We are not allowing this under any circumstances.
 *  We will not take responsibility for the consequences relating to
 *  academic dishonesty and/or plagarism.
 */

package SearchDb;

import javax.naming.NamingException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rbtlong on 6/2/15.
 */
public class DbCtx {

    private DbCtx() {}

    static PreparedStatement
            insertQueryTfIdf=null,
            insertQuerySVD=null,
            getdf=null,
            suggestText = null;

    static Connection conn;

    public static Connection getConn()
            throws ClassNotFoundException, SQLException, NamingException {
        if(conn == null) {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/121Crawler", "cs121", "cs121");
        }
        return conn;
    }

    public static PreparedStatement createQueryTfIdf()
            throws SQLException, ClassNotFoundException, NamingException {
        return getConn().prepareStatement("create table if not exists QueryTfIdf(termid int,score double);");
    }

    public static PreparedStatement dropQueryTfIdf()
            throws SQLException, ClassNotFoundException, NamingException {
        return getConn().prepareStatement("drop table if exists QueryTfIdf;");
    }

    public static PreparedStatement PopQueryTfIdf(int id, double score)
            throws SQLException, ClassNotFoundException, NamingException {
             return getConn().prepareStatement("insert into QueryTfIdf values("+id+" ,"+score+");");

    }

    public static PreparedStatement getDocIdRankingsTfIdf(int lim, int offset)
            throws SQLException, ClassNotFoundException, NamingException {
        String sql="select W.id, W.url, W.noramlized_url, R.score from (" +
                "select docid, sum(F.score*Q.score) score from TfIdf_TrimmedComplete F, " +
                "QueryTfIdf  Q WHERE " +
                "F.termid=Q.termid group by docid order by sum(F.score*Q.score) " +
                "desc limit "+offset+", "+lim+") " +
                "R, Website W where W.id = R.docid;";
        return getConn().prepareStatement(sql);
    }



    public static PreparedStatement getDocIdRankingsLSA(int lim, int offset)
            throws SQLException, ClassNotFoundException, NamingException {
        return getConn().prepareStatement(
                "select W.id, W.url, W.noramlized_url, R.score from (" +
                        "select docid, sum(F.score*Q.score) score " +
                        "from FinalTfIdf_SVD F, QueryLSA Q WHERE " +
                        "F.CId=Q.CId group by docid order by sum(F.score*Q.score) " +
                        "desc limit " + offset + " , " + lim + ") " +
                        "R, Website W where W.id = R.docid;");
    }

    public static PreparedStatement getDocIdRankingLength()
            throws SQLException, NamingException, ClassNotFoundException {
        return getConn().prepareStatement(
                "select count(*) from (" +
                "select docid from FinalTfIdf_complete F, QueryTfIdf Q WHERE " +
                "F.term=Q.term group by docid order by sum(F.score*Q.score)) as a;");
    }

    public static PreparedStatement getDocIdRankingLengthLSA()
            throws SQLException, NamingException, ClassNotFoundException {
        return getConn().prepareStatement(
                "select count(*) from (" +
                        "select docid from FinalTfIdf_SVD F, QueryLSA Q WHERE " +
                        "F.CId=Q.CId group by docid order by sum(F.score*Q.score));");
    }

    public static PreparedStatement getDF(int termid)
            throws SQLException, ClassNotFoundException, NamingException {
        if(getdf==null){
            getdf=getConn().prepareStatement("select doc_freq from DocFreq_CoreTextTrimmed D" +
                    " where D.termid=?;");
        }
        getdf.setInt(1, termid);
        return getdf;
    }
    public static PreparedStatement getID(String str)
            throws SQLException, ClassNotFoundException, NamingException {
        String sql="select id from Terms_Trimmed T" +
                " where Binary T.term = \""+ str +"\" ;";
        return getConn().prepareStatement(sql);

    }
    public static PreparedStatement populateQueryLSA()
            throws SQLException, ClassNotFoundException, NamingException {
            insertQuerySVD= getConn().prepareStatement(
                    "insert into QueryLSA " +
                    "select m.CId, sum(m.weight*Q.score) from M m, QueryTfIdf Q where " +
                    "m.termid = Q.termid group by m.CId;");
        return insertQuerySVD;
    }
    public static PreparedStatement createQueryLSA()
            throws SQLException, ClassNotFoundException, NamingException {
        return getConn().prepareStatement("create table if not exists QueryLSA(CId int,score double);");
    }

    public static PreparedStatement dropQueryLSA()
            throws SQLException, ClassNotFoundException, NamingException {
        return getConn().prepareStatement("drop table if exists QueryLSA;");
    }

    public static List<String> fetchSuggestions(String term, int limit)
            throws SQLException, NamingException, ClassNotFoundException {
        if(suggestText == null)
            suggestText = getConn().prepareStatement(
                    "SELECT DISTINCT T.term FROM Terms_Trimmed T " +
                    "WHERE T.term like ? " +
                    "limit 100; ");
        suggestText.setString(1, "%" + term +"%" );

        ResultSet rs = suggestText.executeQuery();
        ArrayList<String> sugg = new ArrayList<>();
        for(int i=0; rs.next() && i < limit; ++i)
            sugg.add(rs.getString("term"));
        return sugg;
    }

}
