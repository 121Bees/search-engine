<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%--
  ~   Copyright © 2015 Team Bees
  ~   Dongguang You - u3503025@hku.hk,
  ~   Robert Long - rbtLong@live.com
  ~
  ~  This program is free software: you can redistribute it and/or modify
  ~  it under the terms of the GNU General Public License as published by
  ~  the Free Software Foundation, either version 3 of the License, or
  ~  (at your option) any later version.
  ~
  ~  This program is distributed in the hope that it will be useful,
  ~  but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~  GNU General Public License for more details.
  ~
  ~  You should have received a copy of the GNU General Public License
  ~  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  ~
  ~  DISCLAIMER
  ~  Please DO NOT copy any of the contents from our source code to use
  ~  for an assignment. We are not allowing this under any circumstances.
  ~  We will not take responsibility for the consequences relating to
  ~  academic dishonesty and/or plagarism.
  --%>

<html>
<head>
  <title>121 Crawler Search Engine</title>
  <%@ include file="includes/BaseIncludes.jsp" %>
</head>
<body >


<table style="margin-left: auto; margin-right: auto; text-align: center;">
  <tbody>
    <tr>
      <td>
        <img src="img/beelogo.png" width="250" height="261" alt=""/>
        <h1>Team Bees Search Engine</h1>

        <form id="mainForm" name="mainForm" method="get" action="/Bees">
          <select name="qMode" id="qMode">
            <option value="tfidf">Tf-Idf</option>
            <option value="lsa">LSA</option>
            <option value="optBoth">Both</option>
          </select>

          <input type="text" id="q" name="q"/>
          <input type="hidden" id="limit" name="limit" value="20" />
          <input type="hidden" id="offset" name="offset" value="0" />
          <input type="submit"/>
          <br/>
        </form>
      </td>
    </tr>
  </tbody>
</table>

<script language="javascript" type="text/javascript">
  function loadAjax(data, txtStatus, objqXHR){
    console.log("loading ajax: " + data);
    var sugg = $.parseJSON(data).suggest;
    $.each(sugg, function(i,l){ console.log("elm " + l); });
    $("#q").autocomplete({source : sugg});
  }

  $(document).ready(function(){
    console.log("doc ready");
    document.getElementById("q").focus();

    $("#q").on('input', function(e){
      console.log("text changed: " + $("#q").val());
      $.get("/QSuggest", { "q" : $("#q").val() }, loadAjax, "html");
    });
  });


</script>

</body>
</html>
