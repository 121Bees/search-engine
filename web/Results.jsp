<%@ page import="java.util.Collection" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%--
  ~   Copyright © 2015 Team Bees
  ~   Dongguang You - u3503025@hku.hk,
  ~   Robert Long - rbtLong@live.com
  ~
  ~  This program is free software: you can redistribute it and/or modify
  ~  it under the terms of the GNU General Public License as published by
  ~  the Free Software Foundation, either version 3 of the License, or
  ~  (at your option) any later version.
  ~
  ~  This program is distributed in the hope that it will be useful,
  ~  but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~  GNU General Public License for more details.
  ~
  ~  You should have received a copy of the GNU General Public License
  ~  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  ~
  ~  DISCLAIMER
  ~  Please DO NOT copy any of the contents from our source code to use
  ~  for an assignment. We are not allowing this under any circumstances.
  ~  We will not take responsibility for the consequences relating to
  ~  academic dishonesty and/or plagarism.
  --%>

<html>
<head>
  <title>Results</title>
  <%@ include file="includes/BaseIncludes.jsp" %>
</head>
<body>

<form name="frm" id="frm">
  <input type="hidden" id="limit" name="limit" />
  <input type="hidden" id="offset" name="offset" />
  <input type="hidden" id="q" name="q" />
</form>

<% if (request.getAttribute("qresult") == null
        || request.getParameter("q") == null) {%>
<h3>No data to present.</h3>
<% } else { %>

<h1>Results . . .</h1>
<%--<h4>About <%= (int)request.getAttribute("qsize") %> results</h4>--%>
<table>
  <tbody>
    <tr>
      <th>#</th><th>DocId</th><th>Score</th><th>Title</th>
    </tr>
    <%
      int row = 1;
      for (String itm : (Collection<String>) request.getAttribute("qresult")) { %>
    <tr class="<%= row % 2 == 0 ? "trOdd" : "trEven" %>">
      <td> <%=row++%> </td>
      <%=itm%>
    </tr>
    <% } %>
  </tbody>
</table>
<%}%>

<h3><a href="index.jsp">Search Again</a></h3>
</body>
</html>
